package wegood.stub;
public class WeGoodMockFactory implements WeGoodFactory {
    public WeGoodStub createWeGood() {
        return new WeGoodMock();
    }
}
