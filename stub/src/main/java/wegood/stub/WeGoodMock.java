package wegood.stub;

import java.time.LocalTime;
import java.time.temporal.ChronoField;

public class WeGoodMock implements WeGoodStub {

	public boolean areWeGood() {
		int hourOfDay = time.get(ChronoField.HOUR_OF_DAY);
		return ((hourOfDay >= 18) || (hourOfDay < 9));
	}

	private LocalTime time = null;

	public void setTime(LocalTime time) throws Exception {
		this.time = time;
	}

	public String getServiceURL() {
		return System.getenv("WEGOOD_SERVICE_URL");
	}
}
