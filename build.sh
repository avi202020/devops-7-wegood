# Optional parameters: DOCKERHUB_USER_ID DOCKERHUB_PASSWORD

mvn -e clean install
if [ $? -ne 0 ]; then
	exit 1
fi

if [ -n "$SONAR_TOKEN" ]; then
	mvn sonar:sonar -pl !features \
		-Dsonar.projectKey=DevOpsForAgileCoaches \
		-Dsonar.host.url=$SONAR_URL \
		-Dsonar.login=$SONAR_TOKEN
fi
if [ $? -ne 0 ]; then
	exit 1
fi

# Push the stub module Jar to Gitlab:



if [ `uname` = 'Linux' ]; then
	mkdir -p scratch
	rm scratch/*
	cp main/target/*.jar scratch
	sudo docker build -t $IMAGE_NAME -f Dockerfile scratch
	if [ $? -ne 0 ]; then
		exit 1
	fi
	# Push image to Dockerhub:
	if [ "$#" -eq 2 ]; then
		sudo docker login -u $1 -p $2
		if [ $? -ne 0 ]; then
			exit 1
		fi
		sudo docker push $IMAGE_NAME
		if [ $? -ne 0 ]; then
			exit 1
		fi
	fi
fi
